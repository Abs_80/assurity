# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Required Software ###
 - Java SDK (8 or above)
 - IntelliJ Community Edition (https://www.jetbrains.com/idea/download/)
 - Apache Maven (https://maven.apache.org/download.cgi)

### Steps to Reproduce ###
- Open IntelliJ and install the cucumber plugin with its dependencies
- On IntelliJ, import the project using the pom.xml file. 
- To execute the test, open the feature file and click the Run button