package tms.StepDefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

public class ApiStepDefs {

    Response response = null;
    final String catDtlUrl = "https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json";

    @When("^I send a GET request to the trade me sandbox$")
    public void iSendAGetToTheTradeMeSandbox() {
        response = given().param("catalogue","false").when().get(catDtlUrl).prettyPeek();
        assertEquals("The request status is not OK", 200,response.getStatusCode());
    }

    @Then("^I see the following values are returned:$")
    public void iSeeTheFollowingValuesAreReturned(DataTable dataTable) {
        if(response != null) {
            List<List<String>> data = dataTable.cells(1);
            for (List<String> field : data) {
                assertFalse(field.get(0) + " is not available on the response.",
                        response.jsonPath().getString(field.get(0)).isEmpty());
                assertEquals(field.get(0) + " should have a value of " + field.get(1),
                        field.get(1), response.jsonPath().getString(field.get(0)));
            }
        } else {
            fail("The response is not available.");
        }
    }

    @And("^I see the (.*) promotion containing (.*)$")
    public void iSeeTheNamePromotionContainingDescription(String name, String desc) {
        if(response != null) {
            List<Map<String, String>> jsonResponse = response.jsonPath().getList("Promotions");

            for (Map<String, String> promotion : jsonResponse) {
                if(promotion.get("Name").equals(name)){
                    assertEquals(name + " promotion should have a description of " + desc,
                            desc, promotion.get("Description"));
                    return;
                }
            }
            fail(name + " promotion is not found");
        } else {
            fail("The response is not available.");
        }
    }
}
