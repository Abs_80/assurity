Feature: Assurity API

  Scenario Outline: Perform Acceptance Criteria checks
    When I send a GET request to the trade me sandbox
    Then I see the following values are returned:
      | Field     | Result         |
      | Name      | Carbon credits |
      | CanRelist | true           |
    And I see the <Name> promotion containing <Description>

    Examples:
      | Name    | Description     |
      | Gallery | 2x larger image |